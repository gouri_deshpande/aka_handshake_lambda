using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Lambda.Core;
using ApiIntegrationHelper;
using ApiIntegrationHelper.Models;
using Handshake_Net.Models;
using Hanshake_Net.Models;
using Newtonsoft.Json;
using RestSharp;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace get_handshake_customer
{
    public class Function
    {
        #region "Variable Declaration"
        EnvironmentVariablesSet objEnv;
        #endregion

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task FunctionHandler(ILambdaContext context)
        {
            List<ErrorLog> lstLog = new List<ErrorLog>();
            try
            {
                //Start : Access Environment Variables
                lstLog.Add(new ErrorLog()
                {
                    Id = Guid.NewGuid(),
                    LogType = "Event",
                    StackTrace = string.Empty,
                    InnerException = string.Empty,
                    Description = "Read Environment Variables"
                });
                objEnv = AccessEnvironmentVariables();
                //End : Access Environment Variables

                //Start : Connect to Handshake API 
                lstLog.Add(new ErrorLog()
                {
                    Id = Guid.NewGuid(),
                    LogType = "Event",
                    StackTrace = string.Empty,
                    InnerException = string.Empty,
                    Description = "Connect to Handshake API for Authentication"
                });
                RestClient client = ConnectToHandshake.ConnectToHandshakeApi(objEnv);
                //End : Connect to Handshake API 

                //Start : Get Data From Customer Endpoint    
                lstLog.Add(new ErrorLog()
                {
                    Id = Guid.NewGuid(),
                    LogType = "Event",
                    StackTrace = string.Empty,
                    InnerException = string.Empty,
                    Description = "Get Customer Endpoint data from Handshake"
                });
                IRestResponse response = ConnectToHandshake.ExecuteEndpoint(client, "customers");
                var _customers = JsonConvert.DeserializeObject<RootObject<Customer>>(response.Content);
                //End : Get Data From Customer Endpoint

                //Start : Save Customer Data to Dynamodb     
                lstLog.Add(new ErrorLog()
                {
                    Id = Guid.NewGuid(),
                    LogType = "Event",
                    StackTrace = string.Empty,
                    InnerException = string.Empty,
                    Description = "Save Customer Endpoint data to  Customer collection."
                });

                using (var db = new AmazonDynamoDBClient(objEnv.AWSAccessKey, objEnv.AWSSecretAccessKey, Amazon.RegionEndpoint.APSoutheast2))
                {
                    //Start : Save Data to Dynamodb     
                    var table = Table.LoadTable(db, objEnv.TableName);

                    foreach (var itm in _customers.objects)
                    {
                        var jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(itm);
                        LambdaLogger.Log(jsonText);
                        var item = Document.FromJson(jsonText);
                        await table.PutItemAsync(item);
                    }
                    //End : Save Data to Dynamodb 
                }
                //End : Save Data to Dynamodb 

                //Start : Save Log
                SaveErrorAndLog(lstLog);
                //End : Save Log
            }
            catch (Exception ex)
            {
                lstLog.Add(new ErrorLog()
                {
                    Id = Guid.NewGuid(),
                    LogType = "Error",
                    StackTrace = ex.StackTrace != null ? ex.StackTrace : string.Empty,
                    InnerException = ex.InnerException != null ? ex.InnerException.Message : ex.Message != null ? ex.Message : string.Empty,
                    Description = "Error occurred"
                });
                //Start : Save Log
                SaveErrorAndLog(lstLog);
                //End : Save Log
                throw ex;
            }
        }

        private EnvironmentVariablesSet AccessEnvironmentVariables()
        {
            ApiIntegrationHelper.Models.EnvironmentVariablesSet objEnv = new ApiIntegrationHelper.Models.EnvironmentVariablesSet();
            objEnv = ConnectToHandshake.AccessEnvironmentVariable();
            return objEnv;
        }
        private async void SaveErrorAndLog(List<ErrorLog> lstLog)
        {
            var ErrorLogException = new
            {
                Id = Guid.NewGuid(),
                Customer = "AKA",//Environment.GetEnvironmentVariable("CustomerName"),
                API = "Handshake",//Environment.GetEnvironmentVariable("ApiName"),
                EndPointName = "Customer",
                ExecutionDateTime = DateTime.Now,
                Log = lstLog
            };
           
            using (var db = new AmazonDynamoDBClient(objEnv.AWSAccessKey, objEnv.AWSSecretAccessKey, Amazon.RegionEndpoint.APSoutheast2))
            {
                var table = Table.LoadTable(db, "AKA_ErrorLog");
                var jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(ErrorLogException);
                //LambdaLogger.Log(jsonText);
                var item = Document.FromJson(jsonText);
                await table.PutItemAsync(item);
            }
        }
    }
}
